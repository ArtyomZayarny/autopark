/**
 * For starting - create car with consumption:
 * var car = Car(5);
 *
 * Car module. Created car with custom consumption.
 * @param consumption {number}
 * @return {{
 *            start: start,
*             riding: riding,
 *            checkGas: checkGas,
  *           refueling: refueling
*           }}
 * @constructor
 */
function Car(consumption,oil,speed) {
    if (!isNumeric(consumption)) {
        showMessage('Wrong consumption', 'error');
        return;
    }

    if (!isNumeric(oil)) {
        showMessage('Wrong oil consumption', 'error');
        return;
    }
    let maxSpeed = speed;
    let oilBalance = 100;
    let gasBalance = 100;

    let gasConsumption = consumption / 100;
    let oilConsumption = oil / 100;

    let gasResidue;
    let oilResidue;

    let gasVolume = 200;
    let oilVolume = 200;
    let ignition = false;
    let ready = false;

    /**
     * Check gas amount after riding.
     * @param distance {number} - Riding distance.
     * @return {number} - Gas amount.
     */
    function gasCheck(distance) {
        if (gasBalance <= 0) {
            return 0;
        }

        let gasForRide = Math.round(distance * gasConsumption);

        return gasBalance - gasForRide;
    }

    //Oil check
    function oilCheck(distance) {
        if (oilBalance <= 0) {
            return 0;
        }

        let oilForRide = Math.round(distance * oilConsumption);

        return oilBalance - oilForRide;
    }



    /**
     * Show message for a user in the console.
     * @param message {string} - Text message for user.
     * @param type {string} - Type of the message (error, warning, log). log by default.
     */
    function showMessage(message, type) {
        let messageText = message ? message : 'Error in program. Please call - 066 083 07 06';

        switch (type) {
            case 'error':
                console.error(messageText);
                break;
            case 'warning':
                console.warn(messageText);
                break;
            case 'log':
                console.log(messageText);
                break;
            default:
                console.log(messageText);
                break;
        }
    }

    /**
     * Check car for ride.
     * @param distance {number} - Ride distance.
     */
    function checkRide(distance) {
        gasResidue = gasCheck(distance);
        oilResidue = oilCheck(distance);
    }

    /**
     * Check value to number.
     * @param n {void} - value.
     * @return {boolean} - Is number.
     */
    function isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    /**
     * Public methods object.
     */
    return {
        /**
         * Start car.
         */
        start: function() {
            ignition = true;
            // Check gas and oil balances
            if (gasBalance <= 0 && oilBalance <= 0) {
                showMessage('You don\'t have gas. Please refuel the car.', 'error');
                ready = false;
                return;
            }

            ready = true;

            showMessage('Ingition', 'log');
        },

        /**
         * Riding function.
         * @param distance {number} - Riding distance.
         */
        riding: function(distance, speed) {
            if ( speed > maxSpeed ) {
                showMessage('Max speed is ' + maxSpeed ,'error');
                return;
            }

            if (!isNumeric(distance)) {
                showMessage('Wrong distance', 'error');
                return;
            }

            if (!ignition) {
                showMessage('You need start car', 'error');
                return;
            }

            if (!ready) {
                ignition = false;
                showMessage('You need gas station', 'error');
                return;
            }

            checkRide(distance);

            let gasDriven = Math.round(gasBalance * gasConsumption * 100);

            let oilDriven = Math.round(oilBalance * oilConsumption * 100);

            let timeDriven = ( distance / speed ).toFixed(1);



            if (gasResidue <= 0) {
                let distanceLeft = Math.round(distance - gasDriven);

                gasBalance = 0;
                let neddedGas = distanceLeft * gasConsumption;

                showMessage('Gas over. You have driven - ' + gasDriven + '. You need ' + + neddedGas + ' liters. ' + distanceLeft + 'km left', 'warning');
            }

            if (oilResidue <= 0) {
                let distanceLeftOil = Math.round(distance - oilDriven);

                oilBalance = 0;
                let neddedOil = distanceLeftOil * oilConsumption;

                showMessage('Oil over. You have driven - ' + oilDriven + '. You need ' + + neddedOil + ' liters. ' + distanceLeftOil + 'km left', 'warning');
            }

            if (gasResidue > 0) {
                gasBalance = gasResidue;
                showMessage('You arrived. Gas balance - ' + gasResidue ,'log');
            }

            if (oilResidue > 0) {
                oilBalance = oilResidue;
                showMessage('You arrived. Oil balance - ' + oilResidue ,'log');
            }
            if (oilResidue > 0 && gasResidue > 0) {
                showMessage('Travel time - ' + timeDriven + 'ч ','log');
            }

        },

        /**
         * Check gas function.
         */
        checkGas: function() {
            showMessage('Gas - ' + gasBalance, 'log');
        },

        checkOil: function() {
            showMessage('Oil - ' + oilBalance, 'log');
        },

        /**
         * Refueling function.
         * @param gas
         */
        refueling: function(gas) {
            if (!isNumeric(gas)) {
                showMessage('Wrong gas amount', 'error');
                return;
            }

            if (gasVolume === gasBalance) {
                showMessage('Gasoline tank is full', 'warning');
            } else if (gasVolume < gasBalance + gas) {
                gasBalance = 200;
                let excess = Math.round(gas - gasBalance);
                showMessage('Gasoline tank is full. Excess - ' + excess, 'log');
            } else {
                gasBalance = Math.round(gasBalance + gas);
                showMessage('Gas balance - ' + gasBalance, 'log');
            }
        },
        refuelingOil: function(oil){
            if (!isNumeric(oil)) {
                showMessage('Wrong oil amount', 'error');
                return;
            }

            if (oilVolume === oilBalance) {
                showMessage('Gasoline tank is full', 'warning');
            } else if (oilVolume < oilBalance + oil) {
                oilBalance = 200;
                let excess = Math.round(oil - oilBalance);
                showMessage('Oil tank is full. Excess - ' + excess, 'log');
            } else {
                oilBalance = Math.round(oilBalance + oil);
                showMessage('oil balance - ' + oilBalance, 'log');
            }
        },
        checkSpeed:function(){
            return maxSpeed;
        }


    };
}
